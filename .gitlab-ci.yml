variables:
  ## Please update `scripts/version.sh` accordingly
  build_deps_image_version: b98d1114e1bafbe50f017e619cbfcede77df0149
  build_deps_image_name: registry.gitlab.com/tezos/opam-repository
  public_docker_image_name: docker.io/${CI_PROJECT_PATH}
  GIT_STRATEGY: fetch
  GIT_DEPTH: 1
  GET_SOURCES_ATTEMPTS: 2
  ARTIFACT_DOWNLOAD_ATTEMPTS: 2

stages:
  - doc
  - build
  - test
  - packaging
  - publish

############################################################
## Stage: build (only MR)                                 ##
############################################################

.build_template: &build_definition
  image: ${build_deps_image_name}:${build_deps_image_version}
  stage: build
  except:
    - master
    - alphanet
    - zeronet
    - mainnet
    - alphanet-staging
    - zeronet-staging
    - mainnet-staging
    - zeronet-snapshots
    - mainnet-snapshots
  before_script:
    - . ./scripts/version.sh
  tags:
    - gitlab-org

check_opam_deps:
  <<: *build_definition
  script:
    - if [ "${build_deps_image_version}" != "${opam_repository_tag}" ] ; then
        echo "Inconsistent dependencies hash between 'scripts/version.sh' and '.gitlab-ci.yml'." ;
        echo "${build_deps_image_version} != ${opam_repository_tag}" ;
        exit 1 ;
      fi
    - ./scripts/opam-check.sh
    - ./scripts/check_opam_test.sh

check_opam_lint:
  <<: *build_definition
  script:
    - find . ! -path "./_opam/*" -name "*.opam" -exec opam lint {} +;

check_linting:
  <<: *build_definition
  script:
    - dune build @runtest_lint

build:
  <<: *build_definition
  script:
    - . ./scripts/version.sh
    - dune build @runtest_dune_template
    - make all build-test
  artifacts:
    name: "$CI_COMMIT_REF_NAME"
    paths:
      - _build
    expire_in: 1 day
  tags:
    - gitlab-org

############################################################
## Stage: test (only MR)                                  ##
############################################################

.test_template: &test_definition
  <<: *build_definition
  stage: test
  dependencies:
    - build
  retry: 2

unit:stdlib:
  <<: *test_definition
  script:
    - dune build @src/lib_stdlib/runtest

unit:stdlib_unix:
  <<: *test_definition
  script:
    - dune build @src/lib_stdlib_unix/runtest

unit:data_encoding:
  <<: *test_definition
  script:
    - dune build @src/lib_data_encoding/runtest

unit:storage:
  <<: *test_definition
  script:
    - dune build @src/lib_storage/runtest

unit:crypto:
  <<: *test_definition
  script:
    - dune build @src/lib_crypto/runtest

unit:shell:
  <<: *test_definition
  script:
    - dune build @src/lib_shell/runtest

unit:p2p:io-scheduler:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_io_scheduler_ipv4

unit:p2p:socket:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_socket_ipv4

unit:p2p:pool:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_pool_ipv4

unit:proto_alpha:lib_protocol:
  <<: *test_definition
  script:
    - dune build @src/proto_alpha/lib_protocol/runtest

unit:proto_alpha:lib_client:
  <<: *test_definition
  script:
    - dune build @src/proto_alpha/lib_client/test/runtest

unit:p2p:peerset:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_peerset

unit:p2p:ipv6set:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_ipv6set

unit:p2p:banned_peers:
  <<: *test_definition
  script:
    - dune build @src/lib_p2p/runtest_p2p_banned_peers

unit:validation:
  <<: *test_definition
  script:
    - dune build @src/lib_validation/runtest

unit:micheline:
  <<: *test_definition
  script:
    - dune build @src/lib_micheline/runtest

############################################################
## Stage: run shell integration tests                     ##
############################################################

# definition for the environment to run all integration tests
.integration_template: &integration_definition
  <<: *test_definition
  dependencies:
    - build
  before_script:
    - make
  tags:
    - gitlab-org

integration:basic.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_basic.sh

integration:contracts.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_contracts.sh

integration:contracts_opcode.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_contracts_opcode.sh

integration:contracts_macros.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_contracts_macros.sh

integration:contracts_mini_scenarios.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_contracts_mini_scenarios.sh

integration:multinode.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_multinode.sh

integration:inject.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_injection.sh

integration:voting.sh:
  <<: *integration_definition
  script:
    - dune build @src/bin_client/runtest_voting.sh

integration:proto:sandbox:
  <<: *integration_definition
  script:
    - dune build @runtest_sandbox

############################################################
## Stage: run OCaml integration tests                     ##
############################################################

integration:sandboxes:voting:
  <<: *integration_definition
  script:
    - ROOT_PATH=$PWD/flextesa-voting-demo-noops dune build @src/bin_flextesa/runtest_sandbox_voting_demo_noops
  artifacts:
    paths:
    - flextesa-voting-demo-noops
    expire_in: 1 day
    when: on_failure
  allow_failure: true # This test uses too much resources for GitLab's workers

integration:sandboxes:acc-baking:
  <<: *integration_definition
  script:
    - ROOT_PATH=$PWD/flextesa-acc-sdb dune build @src/bin_flextesa/runtest_sandbox_accusations_simple_double_baking
  artifacts:
    paths:
    - flextesa-acc-sdb
    expire_in: 1 day
    when: on_failure

integration:sandboxes:acc-endorsement:
  <<: *integration_definition
  script:
    - ROOT_PATH=$PWD/flextesa-acc-sde dune build @src/bin_flextesa/runtest_sandbox_accusations_simple_double_endorsing
  artifacts:
    paths:
    - flextesa-acc-sde
    expire_in: 1 day
    when: on_failure

############################################################
## Stage: run python integration tests                    ##
############################################################

##BEGIN_INTEGRATION_PYTHON##
integration:baker_endorser:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_baker_endorser.py

integration:basic:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_basic.py

integration:contract:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_contract.py

integration:contract_baker:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_contract_baker.py

integration:cors:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_cors.py

integration:injection:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_injection.py

integration:many_bakers:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_many_bakers.py

integration:many_nodes:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_many_nodes.py

integration:mempool:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_mempool.py

integration:multinode:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_multinode.py

integration:rpc:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_rpc.py

integration:tls:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_tls.py

integration:voting:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_voting.py

integration:double_endorsement:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_double_endorsement.py

integration:snapshot:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_multinode_snapshot.py

integration:proto_demo_noops:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_proto_demo_noops_manual_bake.py

integration:fork:
  <<: *integration_definition
  script:
    - pytest tests_python/tests/test_fork.py

##END_INTEGRATION_PYTHON##

############################################################
## Stage: run doc integration tests                       ##
############################################################

documentation:build:
  <<: *test_definition
  script:
    - make doc-html

documentation:linkcheck:
  <<: *test_definition
  script:
    - make doc-html-and-linkcheck
  allow_failure: true

############################################################
## Stage: building opam packages (only master and *opam*) ##
############################################################

.opam_template: &opam_definition
  image: ${build_deps_image_name}:opam--${build_deps_image_version}
  stage: packaging
  dependencies: []
  only:
    - master
    - /^.*opam.*$/
  script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
  tags:
    - gitlab-org

##BEGIN_OPAM##
opam:00:ocplib-json-typed:
  <<: *opam_definition
  variables:
    package: ocplib-json-typed

opam:01:ocplib-json-typed-bson:
  <<: *opam_definition
  variables:
    package: ocplib-json-typed-bson

opam:02:tezos-stdlib:
  <<: *opam_definition
  variables:
    package: tezos-stdlib

opam:03:tezos-data-encoding:
  <<: *opam_definition
  variables:
    package: tezos-data-encoding

opam:04:ocplib-resto:
  <<: *opam_definition
  variables:
    package: ocplib-resto

opam:05:tezos-error-monad:
  <<: *opam_definition
  variables:
    package: tezos-error-monad

opam:06:ocplib-resto-directory:
  <<: *opam_definition
  variables:
    package: ocplib-resto-directory

opam:07:blake2:
  <<: *opam_definition
  variables:
    package: blake2

opam:08:hacl:
  <<: *opam_definition
  variables:
    package: hacl

opam:09:secp256k1:
  <<: *opam_definition
  variables:
    package: secp256k1

opam:10:tezos-clic:
  <<: *opam_definition
  variables:
    package: tezos-clic

opam:11:tezos-rpc:
  <<: *opam_definition
  variables:
    package: tezos-rpc

opam:12:uecc:
  <<: *opam_definition
  variables:
    package: uecc

opam:13:tezos-crypto:
  <<: *opam_definition
  variables:
    package: tezos-crypto

opam:14:tezos-event-logging:
  <<: *opam_definition
  variables:
    package: tezos-event-logging

opam:15:tezos-micheline:
  <<: *opam_definition
  variables:
    package: tezos-micheline

opam:16:lmdb:
  <<: *opam_definition
  variables:
    package: lmdb

opam:17:tezos-base:
  <<: *opam_definition
  variables:
    package: tezos-base

opam:18:ocplib-resto-cohttp:
  <<: *opam_definition
  variables:
    package: ocplib-resto-cohttp

opam:19:pbkdf:
  <<: *opam_definition
  variables:
    package: pbkdf

opam:20:irmin-lmdb:
  <<: *opam_definition
  variables:
    package: irmin-lmdb

opam:21:tezos-shell-services:
  <<: *opam_definition
  variables:
    package: tezos-shell-services

opam:22:tezos-stdlib-unix:
  <<: *opam_definition
  variables:
    package: tezos-stdlib-unix

opam:23:ocplib-resto-cohttp-client:
  <<: *opam_definition
  variables:
    package: ocplib-resto-cohttp-client

opam:24:tezos-rpc-http:
  <<: *opam_definition
  variables:
    package: tezos-rpc-http

opam:25:bip39:
  <<: *opam_definition
  variables:
    package: bip39

opam:26:tezos-storage:
  <<: *opam_definition
  variables:
    package: tezos-storage

opam:27:ledgerwallet:
  <<: *opam_definition
  variables:
    package: ledgerwallet

opam:28:tezos-rpc-http-client:
  <<: *opam_definition
  variables:
    package: tezos-rpc-http-client

opam:29:tezos-client-base:
  <<: *opam_definition
  variables:
    package: tezos-client-base

opam:30:ledgerwallet-tezos:
  <<: *opam_definition
  variables:
    package: ledgerwallet-tezos

opam:31:tezos-rpc-http-client-unix:
  <<: *opam_definition
  variables:
    package: tezos-rpc-http-client-unix

opam:32:tezos-signer-services:
  <<: *opam_definition
  variables:
    package: tezos-signer-services

opam:33:tezos-protocol-environment-sigs:
  <<: *opam_definition
  variables:
    package: tezos-protocol-environment-sigs

opam:34:tezos-p2p:
  <<: *opam_definition
  variables:
    package: tezos-p2p

opam:35:tezos-signer-backends:
  <<: *opam_definition
  variables:
    package: tezos-signer-backends

opam:36:tezos-protocol-environment:
  <<: *opam_definition
  variables:
    package: tezos-protocol-environment

opam:37:tezos-client-commands:
  <<: *opam_definition
  variables:
    package: tezos-client-commands

opam:38:tezos-protocol-compiler:
  <<: *opam_definition
  variables:
    package: tezos-protocol-compiler

opam:39:tezos-client-base-unix:
  <<: *opam_definition
  variables:
    package: tezos-client-base-unix

opam:40:tezos-protocol-alpha:
  <<: *opam_definition
  variables:
    package: tezos-protocol-alpha

opam:41:tezos-shell-context:
  <<: *opam_definition
  variables:
    package: tezos-shell-context

opam:42:tezos-client-alpha:
  <<: *opam_definition
  variables:
    package: tezos-client-alpha

opam:43:tezos-protocol-updater:
  <<: *opam_definition
  variables:
    package: tezos-protocol-updater

opam:44:tezos-baking-alpha:
  <<: *opam_definition
  variables:
    package: tezos-baking-alpha

opam:45:tezos-protocol-genesis:
  <<: *opam_definition
  variables:
    package: tezos-protocol-genesis

opam:46:ocplib-resto-json:
  <<: *opam_definition
  variables:
    package: ocplib-resto-json

opam:47:tezos-validation:
  <<: *opam_definition
  variables:
    package: tezos-validation

opam:48:ocplib-resto-cohttp-server:
  <<: *opam_definition
  variables:
    package: ocplib-resto-cohttp-server

opam:49:tezos-protocol-demo-noops:
  <<: *opam_definition
  variables:
    package: tezos-protocol-demo-noops

opam:50:tezos-baking-alpha-commands:
  <<: *opam_definition
  variables:
    package: tezos-baking-alpha-commands

opam:51:tezos-client-alpha-commands:
  <<: *opam_definition
  variables:
    package: tezos-client-alpha-commands

opam:52:tezos-client-genesis:
  <<: *opam_definition
  variables:
    package: tezos-client-genesis

opam:53:ocplib-ezresto:
  <<: *opam_definition
  variables:
    package: ocplib-ezresto

opam:54:tezos-embedded-protocol-alpha:
  <<: *opam_definition
  variables:
    package: tezos-embedded-protocol-alpha

opam:55:tezos-shell:
  <<: *opam_definition
  variables:
    package: tezos-shell

opam:56:tezos-protocol-alpha-parameters:
  <<: *opam_definition
  variables:
    package: tezos-protocol-alpha-parameters

opam:57:tezos-rpc-http-server:
  <<: *opam_definition
  variables:
    package: tezos-rpc-http-server

opam:58:tezos-embedded-protocol-demo-noops:
  <<: *opam_definition
  variables:
    package: tezos-embedded-protocol-demo-noops

opam:59:tezos-embedded-protocol-genesis:
  <<: *opam_definition
  variables:
    package: tezos-embedded-protocol-genesis

opam:60:tezos-endorser-alpha-commands:
  <<: *opam_definition
  variables:
    package: tezos-endorser-alpha-commands

opam:61:tezos-client:
  <<: *opam_definition
  variables:
    package: tezos-client

opam:62:ocplib-ezresto-directory:
  <<: *opam_definition
  variables:
    package: ocplib-ezresto-directory

opam:63:tezos-accuser-alpha:
  <<: *opam_definition
  variables:
    package: tezos-accuser-alpha

opam:64:tezos-mempool-alpha:
  <<: *opam_definition
  variables:
    package: tezos-mempool-alpha

opam:65:tezos-tooling:
  <<: *opam_definition
  variables:
    package: tezos-tooling

opam:66:tezos-protocol-alpha-tests:
  <<: *opam_definition
  variables:
    package: tezos-protocol-alpha-tests

opam:67:tezos-alpha-test-helpers:
  <<: *opam_definition
  variables:
    package: tezos-alpha-test-helpers

opam:68:tezos-endorser-alpha:
  <<: *opam_definition
  variables:
    package: tezos-endorser-alpha

opam:69:tezos-accuser-alpha-commands:
  <<: *opam_definition
  variables:
    package: tezos-accuser-alpha-commands

opam:70:tezos-baker-alpha:
  <<: *opam_definition
  variables:
    package: tezos-baker-alpha

opam:71:tezos-network-sandbox:
  <<: *opam_definition
  variables:
    package: tezos-network-sandbox

opam:72:tezos-signer:
  <<: *opam_definition
  variables:
    package: tezos-signer

opam:73:tezos-node:
  <<: *opam_definition
  variables:
    package: tezos-node

opam:74:ocplib-json-typed-browser:
  <<: *opam_definition
  variables:
    package: ocplib-json-typed-browser


##END_OPAM##



############################################################
## Stage: publish                                         ##
############################################################

publish:docker:
  image: docker:latest
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  stage: publish
  only:
    - master@tezos/tezos
    - alphanet@tezos/tezos
    - zeronet@tezos/tezos
    - mainnet@tezos/tezos
    - alphanet-staging@tezos/tezos
    - zeronet-staging@tezos/tezos
    - mainnet-staging@tezos/tezos
    - zeronet-snapshots@tezos/tezos
    - mainnet-snapshots@tezos/tezos
  before_script:
    - apk add git
    - mkdir ~/.docker || true
    - echo "${CI_DOCKER_AUTH}" > ~/.docker/config.json
  script:
    - LAST_COMMIT_DATE_TIME=$(git log --pretty=format:"%cd" -1 --date="format:%Y%m%d%H%M%S" 2>&1)
    - ./scripts/create_docker_image.sh
        "${public_docker_image_name}" "${CI_COMMIT_REF_NAME}"
    - docker push "${public_docker_image_name}:${CI_COMMIT_REF_NAME}"
    - docker tag "${public_docker_image_name}:${CI_COMMIT_REF_NAME}" "${public_docker_image_name}:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
    - docker push "${public_docker_image_name}:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
  tags:
    - safe_docker

publish:doc:
  image: ${build_deps_image_name}:${build_deps_image_version}
  stage: doc
  only:
    - master@tezos/tezos
    - alphanet@tezos/tezos
    - zeronet@tezos/tezos
    - mainnet@tezos/tezos
  before_script:
    - sudo apk add --no-cache openssh-client rsync
    - echo "${CI_PK_GITLAB_DOC}" > ~/.ssh/id_ed25519
    - echo "${CI_KH}" > ~/.ssh/known_hosts
    - chmod 400 ~/.ssh/id_ed25519
  script:
    - make doc-html
    - git clone git@gitlab.com:${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAMESPACE}.gitlab.io gitlab.io
    - rsync --recursive --links --perms --delete --verbose
        --exclude=.doctrees
        docs/_build/ gitlab.io/public/"${CI_COMMIT_REF_NAME}"
    - cd gitlab.io
    - if [ -z "$(git status -s)" ] ; then
        echo "Nothing to commit!" ;
      else
        git add public/"${CI_COMMIT_REF_NAME}" ;
        git commit -m "Import doc for ${CI_COMMIT_REF_NAME} (${CI_COMMIT_SHA})" ;
        git push origin master ;
      fi
  tags:
    - gitlab-org
